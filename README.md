# Text Content Tests

## Introduction

Those are tests that you can add to your langlib or gamerules repositories.

Repository created during my full-time job in NetEnt company in Krakow, Poland.

## Authors
- [Piotr Proc](mailto:piotr.proc@netent.com)
- Christian Andolf 
