/* eslint-disable consistent-this */
const assert = require("assert");
const { describe, it } = require("mocha");
const { readFileFromPath, validateFile } = require("./utils/file");
const { getTemplateFileKeys } = require("./utils/get/getTemplateKeys");
const { templates } = require("./utils/get/getConfig");
const { skipIfNeededWithContext, getNoOfAllowableErrorsWithContext } = require("./utils/mocha/mocha-encapsulate");
const languages = require("./utils/get/getAllTexts");
const englishKeys = languages.en.map((val) => val.id);

describe("[id=templates] templates files", function() {
    const me = this;

    beforeEach(function() {
        skipIfNeededWithContext.call(this);
    });

    context("for mobile", function() {
        it("[id=redundantKeysOnMobile] should have no language keys that are not used", function() {
            const test = this;
            const data = readFileFromPath(templates.mobile);

            me.checkNoLanguageKeysUnused(test, data);
        });

        it("[id=missingKeysOnMobile] should not be missing keys from language", function() {
            const test = this;
            const data = readFileFromPath(templates.mobile);

            me.checkMissingKeys(test, data);
        });

        it("[id=validHtmlOnMobile] should have valid HTML code in template", async function() {
            const test = this;
            const messages = await validateFile(templates.mobile);

            me.checkTemplateHtmlErrors(test, messages);
        });
    });

    context("for desktop", function() {
        it("[id=redundantKeysOnMobile] should have no language keys that are not used", function() {
            const test = this;
            const data = readFileFromPath(templates.desktop);

            me.checkNoLanguageKeysUnused(test, data);
        });

        it("[id=missingKeysOnMobile] should not be missing keys from language", function() {
            const test = this;
            const data = readFileFromPath(templates.desktop);

            me.checkMissingKeys(test, data);
        });

        it("[id=validHtmlOnMobile] should have valid HTML code in template", async function() {
            const test = this;
            const messages = await validateFile(templates.desktop);

            me.checkTemplateHtmlErrors(test, messages);
        });
    });

    me.checkNoLanguageKeysUnused = (test, data) => {
        const result = englishKeys.filter((element) => {
            return data.indexOf(element) === -1;
        });

        assert.ok(result.length === getNoOfAllowableErrorsWithContext.call(test), `Following keys are not used in template: ${result}`);
    };

    me.checkMissingKeys = (test, data) => {
        const result = getTemplateFileKeys(data).filter((element) => {
            return englishKeys.indexOf(element) === -1;
        });

        assert.ok(result.length === getNoOfAllowableErrorsWithContext.call(test), `Following keys are missing from template: ${result}`);
    };

    me.checkTemplateHtmlErrors = (test, messages) => {
        for (const message of messages) {
            console.error(message);
        }

        assert.ok(messages.length === getNoOfAllowableErrorsWithContext.call(test), `There were ${messages.length} errors while validating template.`);
    };
});
