const assert = require("assert");
const { describe, it } = require("mocha");
const iterator = require("./utils/iterator");
const languages = require("./utils/get/getAllTexts");
const iteratee = iterator(languages);
const { skipIfNeeded, getNoOfAllowableErrors } = require("./utils/mocha/mocha-encapsulate");

describe("[id=ids] text ids", function() {
    beforeEach(function() {
        skipIfNeeded.call(this);
    });

    it("[id=idWrittenDifferently] should have no id's written differently than in English version", function() {
        const englishKeys = languages.en.map((val) => val.id);

        let errors = 0;

        iteratee((val, index, language) => {
            const result = englishKeys.indexOf(val.id);

            if (result === -1) {
                errors++;
                console.error(`${val.id} in ${language} does not exist in English.`);
            }
        });

        assert.ok(errors === getNoOfAllowableErrors.call(this), `There are ${errors} id's that doesn't exist in English version.`);
    });
});
