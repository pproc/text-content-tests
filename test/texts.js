const assert = require("assert");
const { describe, it } = require("mocha");
const iterator = require("./utils/iterator");
const languages = require("./utils/get/getAllTexts");
const iteratee = iterator(languages);
const { skipIfNeeded, getNoOfAllowableErrors } = require("./utils/mocha/mocha-encapsulate");

describe("[id=texts] texts itself", function() {
    beforeEach(function() {
        skipIfNeeded.call(this);
    });

    it("[id=emptyTexts] should have no empty text strings", function() {
        let errors = 0;

        iteratee((val, index, language) => {
            if (!val.text) {
                errors++;
                console.error(`${val.id} in ${language} is empty.`);
            }
        });

        assert.ok(errors === getNoOfAllowableErrors.call(this), `There are ${errors} empty texts.`);
    });

    it("[id=blankCharacters] should have no blank characters in beginning or end of string", function() {
        let errors = 0;

        iteratee((val, index, language) => {
            if (val.text.length !== val.text.trim().length) {
                errors++;
                console.error(`${val.id} in ${language} has blank characters: ${val.text}`);
            }
        });

        assert.ok(errors === getNoOfAllowableErrors.call(this), `There are ${errors} texts with blank characters in beginning or end of string.`);
    });

    it("[id=multipleSpaces] should have no multiple spaces", function() {
        const regex = /\s\s+/g;
        let errors = 0;

        iteratee((val, index, language) => {
            if (regex.exec(val.text) !== null) {
                errors++;
                console.error(`${val.id} in ${language} has multiple spaces: ${val.text}`);
            }
        });

        assert.ok(errors === getNoOfAllowableErrors.call(this), `There are ${errors} texts with multiple spaces.`);
    });
});
