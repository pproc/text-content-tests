const assert = require("assert");
const { describe, it } = require("mocha");
const iterator = require("./utils/iterator");
const languages = require("./utils/get/getAllTexts");
const iteratee = iterator(languages);
const { skipIfNeeded, getNoOfAllowableErrors } = require("./utils/mocha/mocha-encapsulate");

describe("[id=punctuation] punctuation rules", function() {
    beforeEach(function() {
        skipIfNeeded.call(this);
    });

    it("[id=noSpacesBeforePunctuationMark] should have no spaces before punctuation mark", function() {
        const regex = /\s[\.,;!?]/g;
        let errors = 0;

        iteratee((val, index, language) => {
            if (regex.exec(val.text) !== null) {
                errors++;
                console.error(`${val.id} in ${language} has some spaces before punctuation mark: ${val.text}`);
            }
        });

        assert.ok(errors === getNoOfAllowableErrors.call(this), `There are ${errors} texts with some spaces before punctuation marks.`);
    });

    it("[id=spaceAfterPunctuationMark] should have space after punctuation mark or the end of the string", function() {
        const regex = /[\.,;!?][a-z]/ig;
        const stringEndingWithPunctuarionMark = /[\.,;!?]$/g;
        let errors = 0;

        iteratee((val, index, language) => {
            if (regex.exec(val.text) !== null && stringEndingWithPunctuarionMark.exec(val.text) === null) {
                errors++;
                console.error(`${val.id} in ${language} has some spaces before punctuation mark: ${val.text}`);
            }
        });

        assert.ok(errors === getNoOfAllowableErrors.call(this), `There are ${errors} texts with some spaces before punctuation marks.`);
    });

    it("[id=doublePunctuationMark] should have no double punctuation marks", function() {
        const punctuationMarks = [",", ";", ":"];
        const exclamationMarks = [".", "!", "?"];
        let errors = 0;

        iteratee((val, index, language) => {
            let mark;
            let errorOccured = false;

            for (let i = 0; i < punctuationMarks.length; i++) {
                mark = "\\" + punctuationMarks[i];
                const regex0 = new RegExp(mark + "{2}");
                const hasTwoMarks = regex0.test(val.text);

                if (hasTwoMarks) {
                    errorOccured = true;
                    console.error(`${val.id} in ${language} has double punctuation: ${val.text}`);
                }
            }

            for (let i = 0; i < exclamationMarks.length; i++) {
                mark = "\\" + exclamationMarks[i];

                const regex1 = new RegExp(mark + "{2}");
                const regex2 = new RegExp(mark + "{3}");
                const regex3 = new RegExp(mark + "{4,}");

                /*
                    Texts meeting regex1 can contain three exclamation marks, what is acceptable.
                    We need to exclude those.
                 */
                const hasTwoMarks = regex1.test(val.text) && !regex2.test(val.text);
                const hasAtLeastFourMarks = regex3.test(val.text);

                if (hasTwoMarks || hasAtLeastFourMarks) {
                    errorOccured = true;
                    console.error(`${val.id} in ${language} has double exclamation: ${val.text}`);
                }
            }

            if (errorOccured) {
                errors++;
            }
        });

        assert.ok(errors === getNoOfAllowableErrors.call(this), `There are ${errors} texts with double punctuation marks.`);
    });
});
