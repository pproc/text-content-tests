/* eslint-disable consistent-this */
const assert = require("assert");
const { describe, it } = require("mocha");
const languages = require("./utils/get/getAllTexts");
const { templates } = require("./utils/get/getConfig");
const { skipIfNeededWithContext, getNoOfAllowableErrorsWithContext } = require("./utils/mocha/mocha-encapsulate");
const { getUntranslatablePhrasesIdsFromGamerules } = require("./utils/regex");
const { getTextsForIds } = require("./utils/get/getTextsForIds");
const { readFileFromPath } = require("./utils/file");
const { getTemplateIds } = require("./utils/get/getTemplateIds");

describe("[id=phrases] not translatable texts", function() {
    const me = this;

    beforeEach(function() {
        skipIfNeededWithContext.call(this);
    });

    context("for desktop", function() {
        it("[id=nonTranslatablePhraseOnDesktop] shouldn't use any not translatable phrases from Gamerules table in all languages on Desktop", function() {
            const test = this;
            const data = readFileFromPath(templates.desktop);

            me.checkUntranslatablePhrases(test, data, "desktop");
        });
    });

    context("for mobile", function() {
        it("[id=nonTranslatablePhraseOnMobile] shouldn't use any not translatable phrases from Gamerules table in all languages on Mobile", function() {
            const test = this;
            const data = readFileFromPath(templates.mobile);

            me.checkUntranslatablePhrases(test, data, "mobile");
        });
    });

    me.checkUntranslatablePhrases = (test, data, platform) => {
        let errors = 0;
        const notTranslatablePhrasesIds = getUntranslatablePhrasesIdsFromGamerules(data);
        const nonTranslatablePhrasesinEnglish = getTextsForIds(languages.en, notTranslatablePhrasesIds);
        const templateTextIds = getTemplateIds(platform);

        for (const key of Object.keys(languages)) {
            const language = languages[key];
            const nonTranslatablePhrases = getTextsForIds(language, notTranslatablePhrasesIds);

            for (const phrase of nonTranslatablePhrases) {
                for (const text of language) {
                    const isForbiddenWordUsed = text.text.split(" ").includes(phrase);
                    const isNotNonTranslatablePhraseItself = !notTranslatablePhrasesIds.includes(text.id);
                    const isNotEquealToEnglishPhrase = !nonTranslatablePhrasesinEnglish.includes(phrase);
                    const isForbiddenWord = isForbiddenWordUsed && isNotNonTranslatablePhraseItself && isNotEquealToEnglishPhrase;
                    const isUsedInTemplate = templateTextIds.includes(text.id);

                    if (isForbiddenWord && isUsedInTemplate) {
                        errors++;
                        console.error(`${text.id} in ${key} uses non translatable text: ${phrase}.`);
                    }
                }
            }
        }

        assert.ok(errors === getNoOfAllowableErrorsWithContext.call(test),
            `There are ${errors} occurences of forbidden phrases in gamerules. ` +
            `Those phrases in English are: ${nonTranslatablePhrasesinEnglish}\n`);
    };
});
