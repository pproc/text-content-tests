const { templates } = require("./getConfig");
const { readFileFromPath } = require("../file");

const getTemplateIds = (template) => {
    const data = readFileFromPath(templates[template]);
    const regex = /<#(.+?)#\/>/g;
    const templateIds = data.match(regex);

    return templateIds.map((id) => {
        return id.substr(2, id.length - 2 - 3);
    });
};

module.exports = {
    getTemplateIds
};
