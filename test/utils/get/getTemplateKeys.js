const getTemplateFileKeys = (data) => {
    const regexp = /{{([\w-]*)}}/g;
    const matches = [];
    let match = regexp.exec(data);

    while (match) {
        matches.push(match[1]);
        match = regexp.exec(data);
    }

    return matches;
};

module.exports = {
    getTemplateFileKeys
};
