const getTextForId = (language, id) => {
    const filterResult = language.filter((text) => {
        return text.id === id;
    });

    return filterResult.length === 1 ? filterResult[0].text : null;
};

const getTextsForIds = (language, ids) => {
    return ids
        .filter((id) => {
            return getTextForId(language, id) !== null;
        }).map((id) => {
            return getTextForId(language, id);
        });
};

module.exports = {
    getTextsForIds
};
