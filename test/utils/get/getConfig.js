const path = require("path");
const fs = require("fs-extra");

const configPath = path.resolve(process.cwd(), "config.js");

try {
    fs.statSync(configPath);
}
catch (e) {
    console.error("Configuration missing, did you forget to add one? [mocha-encapsulate.js]");
}

module.exports = require(configPath);
