const path = require("path");
const fs = require("fs-extra");

const configPath = path.resolve(process.cwd(), "index.js");

try {
    fs.statSync(configPath);
}
catch (e) {
    console.error("Configuration missing, did you forget to add one? [validator.js]");
}

module.exports = require(configPath);
