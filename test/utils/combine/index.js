const _ = require("lodash");

const combineTexts = (object1, object2) => {
    return _.mergeWith({}, object1, object2, (value1, value2) => {
        if (_.isArray(value1)) {
            const outcome = value1.concat(value2);

            return outcome.filter((element1, element2) => {
                return element1.id !== element2.id;
            });
        }

        // Undefined forces lodash to use the default merge strategy
        // eslint-disable-next-line no-undefined
        return undefined;
    });
};

module.exports = combineTexts;
