/* eslint-disable no-undefined */
const { config } = require("../get/getConfig");

const getNoOfAllowableErrors = function(withContext = false) {
    const parentTitle = withContext ? this.test.parent.parent.title : this.test.parent.title;
    const parentId = _getIdFromTitle(parentTitle);
    const testId = _getIdFromTitle(this.test.title);

    return config[parentId][testId].allowableNoOfErros !== undefined ? config[parentId][testId].allowableNoOfErros : 0;
};

const getNoOfAllowableErrorsWithContext = function() {
    return getNoOfAllowableErrors.call(this, true);
};

const skipIfNeeded = function(withContext = false) {
    const parentTitle = withContext ? this.currentTest.parent.parent.title : this.currentTest.parent.title;
    const parentId = _getIdFromTitle(parentTitle);
    const testId = _getIdFromTitle(this.currentTest.title);

    _skipScenario.call(this, parentId, testId);
};

const skipIfNeededWithContext = function() {
    return skipIfNeeded.call(this, true);
};

const _skipScenario = function(testId, scenarioId) {
    const testObject = config[testId];

    if (!testObject) {
        this.skip();
        return;
    }

    if (testObject[scenarioId].use === true) {
        return;
    }

    this.skip();
};

const _getIdFromTitle = function(title) {
    // extracts id from test title
    const regex = /^\[id=(.+)\]/g;
    const regexResult = regex.exec(title);

    if (regexResult.length !== 2) {
        return null;
    }

    return regexResult[1];
};

module.exports = {
    skipIfNeeded,
    skipIfNeededWithContext,
    getNoOfAllowableErrors,
    getNoOfAllowableErrorsWithContext
};
