const iterator = (languages) => {
    return (callback) => {
        Object.keys(languages).forEach((key) => {
            languages[key].forEach((val, i) => {
                callback(val, i, key);
            });
        });
    };
};

module.exports = iterator;
