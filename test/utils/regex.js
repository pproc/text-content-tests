const languages = require("./get/getAllTexts");

const getUntranslatablePhrasesIdsFromGamerules = (data) => {
    const tableRegex = /<table.*?>.+?<\/table>/gm;
    const tableRegexResult = data.replace(/\r/g, "").match(tableRegex);

    const lastTable = tableRegexResult[tableRegexResult.length - 1];

    const tableValuesRegex = /<td><#(.+?)#\/><\/td>/g;

    const notTranslatablePhrases = [];
    let tableValue = tableValuesRegex.exec(lastTable);

    while (tableValue) {
        notTranslatablePhrases.push(tableValue[1]);
        tableValue = tableValuesRegex.exec(lastTable);
    }

    return notTranslatablePhrases;
};

const checkRegexInTexts = (lang, regex, callback) => {
    for (const line of lang) {
        const text = line.text;
        const tags = _runRegex(text, regex);

        tags.forEach((tag) => {
            const tagRegex = _convertTextToRegex(tag);

            Object.keys(languages).forEach((isoCode) => {
                const language = languages[isoCode];
                const otherLangText = language.filter((x) => x.id === line.id)[0];

                callback(otherLangText.id, otherLangText.text, tagRegex, tag, isoCode);
            });
        });
    }
};

const _runRegex = (text, regex) => {
    const tags = [];
    let regexResult = regex.exec(text);

    while (regexResult !== null) {
        tags.push(regexResult[0]);
        regexResult = regex.exec(text);
    }

    return tags;
};

const _convertTextToRegex = (text) => {
    const tag = text.replace("{", "\\{").replace("}", "\\}");

    return new RegExp(tag);
};

module.exports = {
    getUntranslatablePhrasesIdsFromGamerules,
    checkRegexInTexts
};
