/* eslint-disable no-undef */
const validator = require("html-validator");

const asyncValidator = (options) => {
    return new Promise((resolve, reject) => {
        validator(options, (error, data) => {
            if (error) {
                reject(error);
            }
            else {
                resolve(data);
            }
        });
    });
};

module.exports = asyncValidator;
