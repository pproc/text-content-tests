const validator = require("./async/validator");
const fs = require("fs-extra");

const readFileFromPath = (path) => {
    const content = fs.readFileSync(path, "utf8");

    return content.replace(/\n/g, "");
};

const validateFile = async (path) => {
    const data = readFileFromPath(path);
    const options = {
        format: "json",
        data
    };
    const ignoredIssues = [
        "Start tag seen without seeing a doctype first. Expected e.g. “<!DOCTYPE html>”.",
        "Element “head” is missing a required instance of child element “title”."
    ];

    const response = await validator(options);

    return response.messages.filter((val) => ignoredIssues.indexOf(val.message) === -1);
};

module.exports = {
    validateFile,
    readFileFromPath
};
