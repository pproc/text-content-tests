exports.phrases = require("./phrases");
exports.tags = require("./tags");
exports.templates = require("./templates");
exports.texts = require("./texts");
exports.ids = require("./ids");
exports.punctuation = require("./punctuation");
