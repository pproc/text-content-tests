const assert = require("assert");
const { describe, it } = require("mocha");
const { checkRegexInTexts } = require("./utils/regex");
const languages = require("./utils/get/getAllTexts");
const { skipIfNeeded, getNoOfAllowableErrors } = require("./utils/mocha/mocha-encapsulate");

const english = languages.en;

describe("[id=tags] tags within texts", function() {
    beforeEach(function() {
        skipIfNeeded.call(this);
    });

    it("[id=htmlTags] should have no tags written differently than in English version", function() {
        let errors = 0;

        checkRegexInTexts(english, /<.*?>/g, (id, text, tagRegex, tag, isoCode) => {
            if (text.search(tagRegex) === -1) {
                errors++;
                console.error(`${id} in ${isoCode} "${text}" has no: "${tag}" tag`);
            }
        });

        assert.ok(errors === getNoOfAllowableErrors.call(this), `There are ${errors} tags written differently than in English version`);
    });

    it("[id=placeholders] should have no placeholders written differently than in English version", function() {
        let errors = 0;

        checkRegexInTexts(english, /{\d+}/g, (id, text, tagRegex, tag, isoCode) => {
            if (text.search(tagRegex) === -1) {
                errors++;
                console.error(`${id} in ${isoCode} "${text}" has no: "${tag}" placeholders`);
            }
        });

        assert.ok(errors === getNoOfAllowableErrors.call(this), `There are ${errors} placeholders written differently than in English version.`);
    });
});
