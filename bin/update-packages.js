// This commit changes version in packages.json to distinguish snaphot packages and released ones
// Every commit on master branch triggers new tagged releases

const fs = require("fs");
const chalk = require("chalk");
const { echo, exec } = require("shelljs");
const semver = require("semver");

echo("# Versions");

const currentBranch = exec("git rev-parse --abbrev-ref HEAD").stdout.replace(/\r?\n|\r/g, "");
const isBetaRelease = currentBranch === "develop";
const isPrerelease = currentBranch !== "master";

const packageJSON = JSON.parse(fs.readFileSync("package.json", "utf-8"));

if (isPrerelease) {
    const nextVersion = semver.inc(packageJSON.version, "patch");
    const gitHash = exec("git rev-parse HEAD").stdout.slice(0, 8);
    const stage = isBetaRelease ? "beta" : "alpha";

    packageJSON.version = `${nextVersion}-${stage}.${gitHash}`;
    fs.writeFileSync("package.json", JSON.stringify(packageJSON, null, 2));
}

echo(`version: ${chalk.green(packageJSON.name)} - ${chalk.green(packageJSON.version)}`);
